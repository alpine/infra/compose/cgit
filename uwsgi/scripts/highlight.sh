#!/bin/sh

BASENAME="$1"
EXTENSION="${BASENAME##*.}"

# fallback to text file
[ "${BASENAME}" = "${EXTENSION}" ] && EXTENSION=txt
[ -z "${EXTENSION}" ] && EXTENSION=txt

# map Makefile and Makefile.* to .mk
[ "${BASENAME%%.*}" = "Makefile" ] && EXTENSION=mk

# map apkbuild to shell
[ "${BASENAME}" = "APKBUILD" ] && EXTENSION=sh

exec highlight --inline-css --force -f -I -O xhtml -S "$EXTENSION" 2>/dev/null
