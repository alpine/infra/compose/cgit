#!/bin/sh
# This script can be used to generate links in commit messages - the first
# sed expression generates links to commits referenced by their SHA1, while
# the second expression generates links to a fictional bugtracker.
#
# To use this script, refer to this file with either the commit-filter or the
# repo.commit-filter options in cgitrc.

exec sed -r -e 's|\b([0-9a-fA-F]{8,40})\b|<a href="./?id=\1">\1</a>|g' \
    -e 's| #([0-9]+)\b| <a href="http://bugs.alpinelinux.org/issues/\1">#\1</a>|g' \
    -e 's|(CVE-[0-9]+-[0-9]+)|<a href="http://cve.mitre.org/cgi-bin/cvename.cgi?name=\1">\1</a>|g' \
    -e "s|GH-([0-9]+)|<a href=\"https://github.com/alpinelinux/$CGIT_REPO_NAME/pull/\1\">GH-\1</a>|g"
