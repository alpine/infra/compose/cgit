#!/bin/sh

set -euo pipefail

: "${MQTT_HOST:=msg.alpinelinux.org}"
: "${MQTT_SSL:=true}"
: "${MQTT_OPTS:=}"
: "${MQTT_TOPICS:=gitab/push/alpine/+ gitab/tag_push/alpine/+}"

for topic in $MQTT_TOPICS; do
	MQTT_OPTS="$MQTT_OPTS -t $topic"
done

case $MQTT_SSL in
	[Tt][Rr][Uu][Ee])
		MQTT_OPTS="$MQTT_OPTS --port 8883 --cafile /etc/ssl/cert.pem" ;;
esac

chown -R git:git /var/git

su-exec git /usr/local/bin/gitsync.sh

su-exec git /usr/bin/mqtt-exec -v -h $MQTT_HOST $MQTT_OPTS -- \
	/usr/local/bin/gitsync.sh
