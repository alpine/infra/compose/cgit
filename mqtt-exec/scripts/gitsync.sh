#!/bin/sh

set -euo pipefail

topic=${1:-}
flock=${flock:-}
url=https://gitlab.alpinelinux.org

update_repo() {
	local project="$1"
	if [ -d "/var/git/${project##*/}.git" ]; then
		echo "Fetching: $project"
		git -C /var/git/"${project##*/}".git remote update --prune
	else
		echo "Cloning: $project"
		git -C /var/git clone --bare "$url/${project}".git
		git -C /var/git/"${project##*/}".git config \
			remote.origin.mirror true
		git -C /var/git/"${project##*/}".git config \
			remote.origin.fetch +refs/heads/*:refs/heads/*
		git -C /var/git/"${project##*/}".git config --add \
			remote.origin.fetch +refs/tags/*:refs/tags/*
	fi
}

if [ -n "$topic" ]; then
	if [ "$flock" != "$topic" ] ; then
		lockfile=/tmp/"${topic//\//_}".lock
		exec env flock="$topic" flock -n $lockfile "$0" "$@"
	fi
	project=${topic#*/}
	update_repo "${project#*/}"
else
	for repo in $(ls /var/git); do
		echo "Fetching: $repo"
		git -C /var/git/"$repo" remote update --prune
	done
fi
